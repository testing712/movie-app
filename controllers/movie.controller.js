const { movie } = require('../models')

class dataController {
    static async list(req, res, next){
        try {
        const Movie = await movie.findAll({
            where: {
                userId: req.user.id
            },
                attributes: ['id','username','password'],
            })
            res.status(200).json(Movie)
            }catch(err){
                next(err)
            }
    }

    static async getById(req, res, next){
        try {
            const Movie = await movie.findOne({
            where: {
                id: req.params.id
                }
            })
            if (!Movie) {
                throw {
                    status: 404,
                    message: 'User not found'
                }
            } else {
                res.status(200).json({
                    message: 'Succesfully get data'
                });
            }
        }catch(err){
                next(err)
            }
    }

    static async create(req, res, next) {
        try {
            const Movie = await movie.create({
                username: req.body.username,
                password: req.body.password,
            })
            res.status(200).json({
                message:'Successfully create movie'
            })
        }catch(err){
            next(err)
        }
    }

    static async update(req, res, next){
        try {
            const Movie = await movie.findOne({
                where: {
                    id: req.params.id
                }
            })
            if (!Movie){
                throw{
                    status : 404,
                    message : 'Id not found'
                }
            }else { 
                await movie.update({
                password: req.body.password
            },{
                where: {
                    id: req.params.id
                }
            })
            res.status(200).json({
                message: 'Successfully Update'
            })
        }
        }catch(err){
            next(err)
        } 
    }

    static async delete(req, res, next) {
        try {
            const Movie = await movie.findOne({
                where: {
                    id: req.params.id
                }
            })
            if (!Movie){
                throw{
                    status : 404,
                    message : 'Id not found'
                }
            }else {await movie.destroy({
            where: {
                id: req.params.id
            }
            })
            res.status(200).json({
                    message: 'Successfully delete movie'
                })
            }
            }catch(err) {
                next(err)
            }
    }
}

module.exports = dataController