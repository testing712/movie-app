'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class MovieFav extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      MovieFav.belongsTo(models.User, { foreignKey: 'userID' })
      MovieFav.belongsTo(models.Movie, { foreignKey: 'movieID' })
    }
  }
  MovieFav.init({
    movieID: DataTypes.INTEGER,
    userID: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'MovieFav',
  });
  return MovieFav;
};
