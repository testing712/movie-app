const express = require('express')
const router = express.Router()
const dataController = require('../controllers/movie.controller')
const jwt = require('jsonwebtoken')

router.get('/',dataController.list)

router.get('/:id',
(req, res, next) => {
    try {
        if (!req.headers.authorization) {
            throw {
                status: 401,
                message: 'Unauthorized request'
            }
        } else {
            const user = jwt.decode(req.headers.authorization)
            if (user) {
                req.user = user
                next()
            } else {
                throw {
                    status: 401,
                    message: 'Unauthorized request'
                }
            }
        }
    } catch (err) {
        next(err)
    }
},
dataController.getById)

router.post('/',
(req, res, next) => {
    try {
        if (!req.headers.authorization) {
            throw {
                status: 401,
                message: 'Unauthorized request'
            }
        } else {
            const user = jwt.decode(req.headers.authorization)
            if (user) {
                req.user = user
                next()
            } else {
            throw {
                status: 401,
                message: 'Unauthorized request'
            }
            }
        }
    } catch (err) {
        next(err)
    }
},
dataController.create)

router.put('/:id',
(req, res, next) => {
    try {
        if (!req.headers.authorization) {
            throw {
                status: 401,
                message: 'Unauthorized request'
            }
        } else {
            const user = jwt.decode(req.headers.authorization)
            if (user) {
                req.user = user
                next()
            } else {
            throw {
                status: 401,
                message: 'Unauthorized request'
            }
            }
        }
    } catch (err) {
        next(err)
    }
},
dataController.update)
router.delete('/:id',
(req, res, next) => {
    try {
        if (!req.headers.authorization) {
            throw {
                status: 401,
                message: 'Unauthorized request'
            }
        } else {
            const user = jwt.decode(req.headers.authorization)
            if (user) {
                req.user = user
                next()
            } else {
            throw {
                status: 401,
                message: 'Unauthorized request'
            }
            }
        }
    } catch (err) {
        next(err)
    }
},
dataController.delete)

module.exports = router