const express = require('express')
const router = express.Router()
const authRoutes = require('./auth.route')
const movieRoutes = require('./movie.route')

router.use('/login', authRoutes)
router.use('/movie', movieRoutes)

module.exports = router